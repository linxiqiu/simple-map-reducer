package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	String outputDir;
	
	Fields myFields = new Fields();

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();

	@Override
	public void cleanup() {
		// Do nothing

	}

	@Override
	public boolean execute(Tuple input) {
		if (!input.isEndOfStream()) {
			System.out.println(getExecutorId() + ": " + input.toString());
			
			String key = input.getStringByField("key");
			String value = input.getStringByField("value");
			try {
				String text = "(" + key + "," + value + ")\n";
				Files.write(Paths.get(outputDir + "/output.txt"), text.getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
			
		}
		
		return true;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		outputDir = stormConf.get("databasedir") + "/" + stormConf.get("outputdirectory");
		
        Path filePath = Paths.get(outputDir);
		if (!Files.exists(filePath)) {
			try {
				Files.createDirectories(filePath);
				System.out.println(filePath.toString() + ": yes");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		File file = new File(filePath.toString(), "output.txt"); // put the file inside the folder
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
