package edu.upenn.cis.stormlite.storage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class StorageImpl {
	private File dir;
	private DPLDbManager dbMgr;

	public StorageImpl(String directory) {
		Path filePath = Paths.get(directory);
		if (!Files.exists(filePath)) {
			try {
				Files.createDirectories(filePath);
				System.out.println("yes");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		dir = new File(filePath.toString());
		try {
			dbMgr = new DPLDbManager(dir);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addAggregator(Aggregator a) {
		AggregatorDAO aDAO = dbMgr.createAggregatorDAO();
		
		Aggregator agg = aDAO.getAggregator(a.getKey());
		if (agg == null)
			aDAO.saveAggregator(a);
		else {
			// Update the aggregator
			agg.getVal().addAll(a.getVal());
			aDAO.saveAggregator(agg);
		}		
		aDAO.close();
	}
	
	public List<Aggregator> getAggregators() {
		List<Aggregator> aggList = new ArrayList<Aggregator>();
		AggregatorDAO aDAO = dbMgr.createAggregatorDAO();
		aggList = aDAO.getAggregators();
		aDAO.close();
		return aggList;
	}
	
	public void close() {
		dbMgr.close();
	}
	
	public void Annihilate() {
		dbMgr.cleanStore();
	}
}
