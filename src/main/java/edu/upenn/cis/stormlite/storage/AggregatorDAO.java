package edu.upenn.cis.stormlite.storage;

import java.util.*;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class AggregatorDAO {
	/* The database manager from which this DAO is created. */
	private final DPLDbManager dbMgr;

	/* The document store. */
	private final EntityStore aggregatorStore;
	
	/* The primary key index. */
	private final PrimaryIndex<String, Aggregator> idIndex;
	
	public AggregatorDAO(DPLDbManager dbManager) throws DatabaseException {
		dbMgr = dbManager;

		StoreConfig aggregatorCfg = new StoreConfig().setTransactional(true);
		aggregatorCfg.setAllowCreate(true);
		aggregatorStore = new EntityStore(dbMgr.env, "Aggregator", aggregatorCfg);
		
		idIndex = aggregatorStore.getPrimaryIndex(String.class, Aggregator.class);
	}
	
	public void close() {
		aggregatorStore.close();
	}
	
	public void saveAggregator(Aggregator a) {
		idIndex.put(dbMgr.getCurrentTxn(), a);
	}
	
	public Aggregator getAggregator(String key) {
		return idIndex.get(key);
	}
	
	public List<Aggregator> getAggregators() {
		ArrayList<Aggregator> allAggregators = new ArrayList<Aggregator>();
		EntityCursor<Aggregator> cursor = idIndex.entities();
		
		for (Aggregator c : cursor) { allAggregators.add(c); }
		cursor.close();
		return allAggregators;
	}
	
}
