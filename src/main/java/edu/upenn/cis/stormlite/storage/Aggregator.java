package edu.upenn.cis.stormlite.storage;

import java.util.Iterator;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import edu.upenn.cis455.mapreduce.ContextImpl;

@Entity
public class Aggregator {
	@PrimaryKey
	private String key;
	
	private List<String> value;
	
	// For DPL only
	public Aggregator() {}
	
	public Aggregator(String key, List<String> value) {
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public List<String> getVal() {
		return this.value;
	}
	
	public Iterator<String> getValIter() {
		return this.value.iterator();
	}
}
