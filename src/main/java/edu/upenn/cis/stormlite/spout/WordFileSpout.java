package edu.upenn.cis.stormlite.spout;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.tuple.Values;

public class WordFileSpout extends FileSpout {
	
	String dir;
    @SuppressWarnings("rawtypes")
    @Override
    public void open(Map conf, TopologyContext context,
                     SpoutOutputCollector collector) {
        this.collector = collector;
        
        String databaseDir = (String)conf.get("databasedir");;
        String inputDir = (String)conf.get("inputdirectory") == "/" ? "" : "/" + (String)conf.get("inputdirectory") + "/";
        
        dir = databaseDir + inputDir;
        try {
            filename = getFilename();
            log.debug("Starting spout for " + filename +
                    " on worker " + getExecutorId());
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
    @Override
    public synchronized boolean nextTuple() {
        if (reader != null && !sentEof) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    log.debug(getExecutorId() + " read from file " + filename + ": " + line);
                    this.collector.emit(new Values<Object>(String.valueOf(inx++), line), getExecutorId());
                } else if (!sentEof) {
                    log.info(getExecutorId() + " finished file " + filename + " and emitting EOS");
                    this.collector.emitEndOfStream(getExecutorId());
                    sentEof = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Thread.yield();
            return true;
        } else
            return false;
    }


	@Override
	public String getFilename() {
		// TODO Auto-generated method stub
		File f = new File(dir);
		
		f = f.listFiles()[0];
		
		return f.toString();
	}
}
