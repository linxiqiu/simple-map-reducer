package edu.upenn.cis455.mapreduce.master;

import java.util.*;

import edu.upenn.cis.stormlite.TopologyContext.STATE;

public class WorkerInfo {
	
	private String port;
	
	private String keysWritten;
	
	private String keysRead;
	
	private String job;
	
	STATE state;
	
	private Map<String, String> result;
	
	public WorkerInfo(String port, STATE state, String job, String keysRead, String keysWritten, Map<String, String> result) {
		this.port = port;
		this.state = state;
		this.job = job;
		this.keysRead = keysRead;
		this.keysWritten = keysWritten;
		this.result = result;
	}
	
	public void setState(STATE s) {
		this.state = s;
	}
	
	public void setKeysWritten(String k) {
		this.keysWritten = k;
	}
	
	public void setKeysRead(String k) {
		this.keysRead = k;
	}
	
	public void setJob(String j) {
		this.job = j;
	}
	
	public void setResult(Map<String, String> r) {
		this.result = r;
	}
	
	private String map2String() {
		String s = "[";
		int length = Math.min(100, result.keySet().size());
		int count = 0;
		for (String key : result.keySet()) {
			s += "(" + key + "," + result.get(key) + ")";
			count++;
			if (count == length)
				break;
			else
				s += ",";
		}
		s += "]";
		return s;
	}
	
	public String toString() {
		//0: port=8001, status=IDLE, job=None, keysRead=0, keysWritten=0, results=[]
		return "port=" + this.port + ", status=" + this.state + ", job=" + this.job + ", keysRead=" + this.keysRead
				+ ", keysWritten=" + this.keysWritten + ", results=" + map2String();
				
	}
	
}
