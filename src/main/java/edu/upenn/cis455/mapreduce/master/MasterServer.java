package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.PrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.WordFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;

public class MasterServer {  
	static Logger log = LogManager.getLogger(MasterServer.class);
	
	private static Map<String, Long> portTimeStamp = new HashMap<>();
	
	private static Map<String, WorkerInfo> workerParams = new HashMap<>();
	
	private static Config config = new Config();
	
	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
	
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");
            
            String content = "<html><head><title>Master</title></head>\r\n";
            int count = 0;
            for (String worker : portTimeStamp.keySet()) {
            	if (System.currentTimeMillis() - portTimeStamp.get(worker) < 30000) {
            		WorkerInfo wi = workerParams.get(worker);
            		content += String.valueOf(count) + ": " + wi.toString() + "<br>\r\n";
            		count++;
            	}
            }
            
            content += "<body>Hi, I am Linxi Qiu! qiul@seas.upenn.edu"
                    + "</body>\r\n"
            		+ "<form method=\"POST\" action=\"/submitjob\">\r\n"
            		+ " Job Name: <input type=\"text\" name=\"jobname\"/><br/>\r\n"
            		+ " Class Name: <input type=\"text\" name=\"classname\"/><br/>\r\n"
            		+ " Input Directory: <input type=\"text\" name=\"input\"/><br/>\r\n"
            		+ " Output Directory: <input type=\"text\" name=\"output\"/><br/>\r\n"
            		+ " Map Threads: <input type=\"text\" name=\"map\"/><br/>\r\n"
            		+ " Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\r\n"
            		+ " <input type=\"submit\" value=\"Submit\">"
            		+ "</form></html>";
            response.body(content);
            
            return response.body();
        });
        
        post("/submitjob", (request, response) -> {
        	/*
        	 *  Configurations
        	 */
            config.put("workerList", "[127.0.0.1:8001,127.0.0.1:8002]");
            config.put("workerIndex", "0");
            config.put("workerNumber", "2");
            // Job name
            config.put("job", request.queryParams("jobname"));
            
            // Class with map function
            config.put("mapClass", request.queryParams("classname"));
            // Class with reduce function
            config.put("reduceClass", request.queryParams("classname"));
            
            // Numbers of executors (per node)
            config.put("spoutExecutors", "1");
            config.put("mapExecutors", request.queryParams("map"));
            config.put("reduceExecutors", request.queryParams("reduce"));
            
            // Directory
            config.put("inputdirectory", request.queryParams("input"));
            config.put("outputdirectory", request.queryParams("output"));
            
        	/*
        	 *  Topology
        	 */
	        FileSpout spout = new WordFileSpout();
	        MapBolt bolt = new MapBolt();
	        ReduceBolt bolt2 = new ReduceBolt();
	        PrintBolt printer = new PrintBolt();
        	
	        TopologyBuilder builder = new TopologyBuilder();

	        // Only one source ("spout") for the words
	        builder.setSpout(WORD_SPOUT, spout, 1);
	        
	        // Parallel mappers, each of which gets specific words
	        builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(request.queryParams("map"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
	        
	        // Parallel reducers, each of which gets specific words
	        builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(request.queryParams("reduce"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

	        // Only use the first printer bolt for reducing to a single point
	        builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);

	        Topology topo = builder.createTopology();
	        
	        WorkerJob job = new WorkerJob(topo, config);
	        
	        ObjectMapper mapper = new ObjectMapper();
	        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
			try {
				String[] workers = WorkerHelper.getWorkers(config);
	
				int i = 0;
				for (String dest: workers) {
			        config.put("workerIndex", String.valueOf(i++));
					if (sendJob(dest, "POST", config, "definejob", 
							mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job definition request failed");
					}
				}
				for (String dest: workers) {
					if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job execution request failed");
					}
				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		        System.exit(0);
			}  
	        
        	return "OK!";
        });
    }
    
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		log.info("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();
		
		return conn;
    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis455.mapreduce.master", Level.DEBUG);
    	
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();

        // TODO: route handler for /workerstatus reports from the workers
        get("/workerstatus", (req, res) -> {
        	TypeReference<Map<String, String>> typeRef 
        	  = new TypeReference<Map<String, String>>() {};
      		ObjectMapper mapper = new ObjectMapper();
      		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
      		
      		String portNum = req.queryParams("port");
        	String status = req.queryParams("status");
        	String job = config.get("job");
        	String mapInput = req.queryParams("mapinput");
        	String mapOutput = req.queryParams("mapoutput");
        	String reduceInput = req.queryParams("reduceinput");
        	String reduceOutput = req.queryParams("reduceoutput");
        	String jsonForMap = req.queryParams("results");
        	Map<String, String> map = mapper.readValue(jsonForMap, typeRef);
        	
        	log.info("received from: " + portNum);
        	System.out.println(map.toString());
        	
        	portTimeStamp.put(portNum, System.currentTimeMillis());
        	if (status.equals("IDLE")) {
        		workerParams.put(portNum, new WorkerInfo(portNum, STATE.IDLE, job, "0", reduceOutput, map));
        	}
        	else if (status.equals("MAPPING")) {
        		workerParams.put(portNum, new WorkerInfo(portNum, STATE.MAPPING, job, mapInput, mapOutput, map));
        	}
        	else if (status.equals("REDUCING")) {
        		workerParams.put(portNum, new WorkerInfo(portNum, STATE.REDUCING, job, reduceInput, reduceOutput, map));
        	}
        	else if (status.equals("INIT")) {
        		workerParams.put(portNum, new WorkerInfo(portNum, STATE.INIT, null, "0", "0", null));
        	}
        	
        	
        	return "";
        });
    }
}

