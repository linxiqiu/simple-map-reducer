package edu.upenn.cis455.mapreduce;

import java.util.*;

import com.sleepycat.persist.model.Persistent;

public class ContextImpl implements Context {
	
	private Map<String, List<String>> ctxtMp;
	
	public ContextImpl(){
		this.ctxtMp = new HashMap<>();
	}
	
	@Override
	public void write(String key, String value, String sourceExecutor) {
		// TODO Auto-generated method stub
		if (ctxtMp.containsKey(key)) {
			List<String> list = ctxtMp.get(key);
			System.out.println(value);
			list.add(value);
			ctxtMp.put(key, list);
		}
		else {
			List<String> list = new ArrayList<String>();
			list.add(value);
			ctxtMp.put(key, list);
		}
	}
	
	public Map<String, List<String>> getMap(){
		return this.ctxtMp;
	}
	
	public int getMapSize() {
		return this.ctxtMp.size();
	}
	
	public void clear() {
		this.ctxtMp.clear();
	}

}
